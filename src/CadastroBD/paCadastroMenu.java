/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CadastroBD;

import BD.ConexaoMYSQL;
import PadroesTela.Grid;
import java.sql.Date;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import PadroesTela.paImprimirPDF;
import java.util.ArrayList;

public class paCadastroMenu extends javax.swing.JDialog {
    
    private ConexaoMYSQL c;
    private String Database;
    private String Tabela;
    private String Titulo;
    private String view;
    private String FiltroMenu;
    private String[] NomeColunas;
    private paCadastroEntradas paEntrada;
    private String[] Columns;
    private int[] FieldTypes;
    private ArrayList Ordenacao;
    private int LinhaSelecionada;
    private paFiltro DadosFiltro;

    public paCadastroMenu(ConexaoMYSQL c,String Database, String Tabela, String view, String Titulo,
            String FiltroMenu, String[] Columns, String[] NomeColunas, paCadastroEntradas paEntrada) {
        
        initComponents();
        this.setModal(true);//impedi abertura de outra tela
        
        this.c = c;
        this.LinhaSelecionada = -1;
        this.view = view;
        this.Tabela = Tabela;
        this.Titulo = Titulo;
        this.FiltroMenu = FiltroMenu;
        this.Columns = Columns;
        this.NomeColunas = NomeColunas;
        this.paEntrada = paEntrada;
        
        //Definir tipos
        this.FieldTypes = new int[this.Columns.length];
        for(int i=0; i<this.Columns.length; i++) {
            try {
                String SQL;
                if (this.view.isEmpty())
                    SQL = "SELECT " + this.Columns[i] + " FROM " + this.Tabela;
                else
                    SQL = "SELECT " + this.Columns[i] + " FROM " + this.view;

                c.setResultSet(SQL);                                                       
                ResultSetMetaData rsmd = c.getResultSet().getMetaData();
                this.FieldTypes[i] = rsmd.getColumnType(1);                    
            }
            catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }//Fim for
        
        this.Ordenacao = new ArrayList();
        
        //paFiltro
        this.DadosFiltro = new paFiltro(this.FieldTypes, this.Columns, this.NomeColunas);
        
        this.setTitle(Titulo);
        this.CriarGrid();
        this.PreencherGrid();
            
    }
    
    //Esse segundo construtor serve apenas para esconder os botões
    public paCadastroMenu(ConexaoMYSQL c,String Database, String Tabela, String view, String Titulo,
            String FiltroMenu, String[] Columns, String[] NomeColunas, paCadastroEntradas paEntrada, 
            String OcultarBotao) {
        
        initComponents();
        this.setModal(true);//impedi abertura de outra tela
        
        this.c = c;
        this.LinhaSelecionada = -1;
        this.view = view;
        this.Tabela = Tabela;
        this.Titulo = Titulo;
        this.FiltroMenu = FiltroMenu;
        this.Columns = Columns;
        this.NomeColunas = NomeColunas;
        this.paEntrada = paEntrada;
        
        //Definir tipos
        this.FieldTypes = new int[this.Columns.length];
        for(int i=0; i<this.Columns.length; i++) {
            try {
                String SQL;
                if (this.view.isEmpty())
                    SQL = "SELECT " + this.Columns[i] + " FROM " + this.Tabela;
                else
                    SQL = "SELECT " + this.Columns[i] + " FROM " + this.view;

                c.setResultSet(SQL);                                                       
                ResultSetMetaData rsmd = c.getResultSet().getMetaData();
                this.FieldTypes[i] = rsmd.getColumnType(1);                    
            }
            catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }//Fim for
        
        this.Ordenacao = new ArrayList();
        
        //paFiltro
        this.DadosFiltro = new paFiltro(this.FieldTypes, this.Columns, this.NomeColunas);
        
        this.setTitle(Titulo);
        this.CriarGrid();
        this.PreencherGrid();
        
        if(OcultarBotao.equalsIgnoreCase("Novo"))
            btnNovo.setVisible(false);
    }
    
    
    private String getOrdenacao(){
        String OrderBY = "";
        
        if(this.Ordenacao.size() > 0){
            OrderBY = " ORDER BY ";
            
            for(int i=0; i<this.Ordenacao.size(); i++){
                for(int j=0; j<this.NomeColunas.length; j++){
                    if(this.Ordenacao.get(i).equals(this.NomeColunas[j])){
                        OrderBY = OrderBY + this.Columns[j];
                        
                        if(i <this.Ordenacao.size() - 1){
                            OrderBY = OrderBY + ", ";
                        }
                        break;
                    }
                }
            }
        }
        return OrderBY;
    }
    
    
    private void CriarGrid(){
        //Definir colunas que não podem ser editadas.
        boolean[] canEdit = new boolean[this.NomeColunas.length];
        for(int i=0; i<this.NomeColunas.length; i++){
            canEdit[i] = false;
        }
        
        //Definir os tipos das colunas (Todas objects)
        Class[] types = new Class[this.NomeColunas.length];
        for(int i=0; i<this.NomeColunas.length; i++){
            types[i] = java.lang.Object.class;
        }
        
        GridDados.setModel(new javax.swing.table.DefaultTableModel(
            //Linhas
            new Object [][] {},
            //Colunas
            this.NomeColunas)
        {
            public Class getColumnClass(int columnIndex){
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex){
                return canEdit[columnIndex];
            }
        });
        
        //Definir todas com tamanho minimo zero
        for(int i=0; i<this.NomeColunas.length; i++){
            GridDados.getColumnModel().getColumn(i).setMinWidth(0);
        }
        
        //Definir o tamanho da coluna codigo
        GridDados.getColumnModel().getColumn(0).setMaxWidth(80);
    }
    
    
    private void PreencherGrid(){//Get Grid
        DefaultTableModel dtm = new DefaultTableModel();
        dtm = (DefaultTableModel) GridDados.getModel();
        
        //Exclui todas as linhas
        //Limpa todo o grid primeiro para que ele possa ser preechido denovo com o novo registro ou com o msm alterado.
        while (dtm.getRowCount() > 0){
            dtm.removeRow(0);
        }
        
        String FiltroPadrao = this.DadosFiltro.GetFiltro();//Sempre lembrar de instanciar a variavel antes de chama-la.
        
        try {//Abaixo comando.
            String SQL;
            String Filtro = "";
            
            if(FiltroPadrao.equals("")){
                if(!this.FiltroMenu.isEmpty())
                    Filtro = " WHERE (" + this.FiltroMenu + ") ";
            }
            else{
                if(!this.FiltroMenu.isEmpty())
                    Filtro = " WHERE (" + this.FiltroMenu + ") AND " + FiltroPadrao;
                else
                    Filtro = " WHERE " + FiltroPadrao;
            }
                
                
            if(this.view.isEmpty())//Novo a partir de ORDER BY
                SQL = "SELECT * FROM " + this.Tabela + Filtro + " ORDER BY cod"+this.Tabela;
            else
                SQL = "SELECT * FROM " + this.view + Filtro;
            
            //Inserir Ordenação
            SQL = SQL + this.getOrdenacao();
            
            this.c.setResultSet(SQL);
            //this.c.getResultSet().first();
            
            if(this.c.getResultSet().first()){
                Object linha[] = new Object[this.Columns.length];
                
                do{    
                    for(int i=0; i<this.Columns.length; i++){
                        
                        String ValorCampo = c.getResultSet().getString(this.Columns[i]);
                        if(this.c.getResultSet().wasNull()){
                            linha[i] = "";
                        }
                        else{
                            //Se o campo for do tipo data, definir padrão brasileiro
                            if (this.FieldTypes[i] == Types.DATE || this.FieldTypes[i] == Types.TIMESTAMP) {                                
                                try {                                                                        
                                    Date data = c.getResultSet().getDate(this.Columns[i]);
                                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                                    ValorCampo = formato.format(data);                         
                                } catch (SQLException e) {
                                    JOptionPane.showMessageDialog(null, e);
                                }   
                            }
                            
                            //Se o campo for real
                            if (this.FieldTypes[i] == Types.REAL || this.FieldTypes[i] == Types.DOUBLE || this.FieldTypes[i] == Types.DECIMAL || this.FieldTypes[i] == Types.FLOAT) {
                                ValorCampo = String.format("%.2f", Float.valueOf(ValorCampo));
                            }
                            
                            //Adicionar no grid o valor do campo
                            linha[i] = ValorCampo;
                        }
                    }
                    
                    dtm.addRow(linha);
                }while(this.c.getResultSet().next());
                
                if(this.LinhaSelecionada >= 0 && this.LinhaSelecionada <= GridDados.getRowCount()-1){
                    GridDados.setRowSelectionInterval(this.LinhaSelecionada, this.LinhaSelecionada);
                }
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        
        //Atualizar Total de registros
        try {
            String SQL;
            
            String Filtro = "";
            if(!this.FiltroMenu.isEmpty()){
                Filtro = " WHERE (" + this.FiltroMenu + ")";
                if(!FiltroPadrao.equals("")){
                    Filtro = " WHERE (" + this.FiltroMenu + ") AND (" +FiltroPadrao +")";
                }
            }
            else{
                if(!FiltroPadrao.equals("")){
                    Filtro = " WHERE (" +FiltroPadrao +")";
                }
            }
            
            if(this.view.isEmpty())
                SQL = "SELECT COUNT(*) AS Total FROM " + this.Tabela + Filtro;
            else
                SQL = "SELECT COUNT(*) AS Total FROM " + this.view + Filtro;
            
            this.c.setResultSet(SQL);
            if(this.c.getResultSet().first())
                lblTotalRegistrosMostrar.setText(this.c.getResultSet().getString("Total"));
            else
                lblTotalRegistrosMostrar.setText("Erro");
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e);  
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        GridDados = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnNovo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnOrdenar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        btnMostrarOcultar = new javax.swing.JButton();
        btnFiltrar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblTotalRegistros = new javax.swing.JLabel();
        lblTotalRegistrosMostrar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        GridDados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(GridDados);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        btnOrdenar.setText("Ordenar");
        btnOrdenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrdenarActionPerformed(evt);
            }
        });

        btnImprimir.setText("Imprimir");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        btnMostrarOcultar.setText("Mostrar/Ocultar Colunas");
        btnMostrarOcultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarOcultarActionPerformed(evt);
            }
        });

        btnFiltrar.setText("Filtrar");
        btnFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFiltrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFiltrar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOrdenar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMostrarOcultar)
                .addContainerGap(493, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNovo)
                    .addComponent(btnEditar)
                    .addComponent(btnExcluir)
                    .addComponent(btnOrdenar)
                    .addComponent(btnImprimir)
                    .addComponent(btnMostrarOcultar)
                    .addComponent(btnFiltrar))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblTotalRegistros.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTotalRegistros.setText("Total de Registros:");

        lblTotalRegistrosMostrar.setText("Mostrar..");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTotalRegistros)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTotalRegistrosMostrar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalRegistros)
                    .addComponent(lblTotalRegistrosMostrar))
                .addGap(0, 9, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        // TODO add your handling code here:
        this.paEntrada.Abrir(0);//Como tem um set visible no final do metodo nao precisa botar embaixo.
        this.LinhaSelecionada = -1;
        this.PreencherGrid();
    }//GEN-LAST:event_btnNovoActionPerformed

    
    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        // TODO add your handling code here:         
        this.c.setDelete(GridDados, this.Tabela);
        this.LinhaSelecionada = -1;
        this.PreencherGrid();
    }//GEN-LAST:event_btnExcluirActionPerformed

    
    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        // TODO add your handling code here:
        new paImprimirPDF(GridDados, "Relatorio de "+Titulo).setVisible(true);
    }//GEN-LAST:event_btnImprimirActionPerformed

    
    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        String Codigo = new Grid().getValor(GridDados, 0);
        
        if(!Codigo.isEmpty()){
            this.paEntrada.Abrir(Integer.parseInt(Codigo));//Como tem um set visible no final do metodo nao precisa botar embaixo.
            this.PreencherGrid();
        } 
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnOrdenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrdenarActionPerformed
        // TODO add your handling code here:
        new paCamposOrdenar(this.Ordenacao, this.NomeColunas).setVisible(true);        
        this.LinhaSelecionada = -1;
        this.PreencherGrid();
    }//GEN-LAST:event_btnOrdenarActionPerformed

    private void btnMostrarOcultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarOcultarActionPerformed
        // TODO add your handling code here:
        new paMostrarCampos(GridDados).setVisible(true);

        //Preencher
        if (this.GridDados.getColumnModel().getColumn(0).getPreferredWidth() > 0) {
            this.GridDados.getColumnModel().getColumn(0).setMaxWidth(80);
            this.GridDados.getColumnModel().getColumn(0).setPreferredWidth(80);
        }
    }//GEN-LAST:event_btnMostrarOcultarActionPerformed

    private void btnFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFiltrarActionPerformed
        // TODO add your handling code here:
        this.DadosFiltro.Abrir(); 
        this.PreencherGrid();
    }//GEN-LAST:event_btnFiltrarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable GridDados;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnFiltrar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnMostrarOcultar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnOrdenar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTotalRegistros;
    private javax.swing.JLabel lblTotalRegistrosMostrar;
    // End of variables declaration//GEN-END:variables
}
