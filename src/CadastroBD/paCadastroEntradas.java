/*     REGRAS PARA HERDAR

        1 - Adiiconar como biblioteca no projeto o projeto padrão;
        2 - Adicionar como biblioteca no projeto o Jar do iText;
        3 - Adiiconar como biblioteca no projeto o driver do MySQL;        
        4 - Herdar esta classe;  
        5 - Chamar o método this.Load(Database, TableName, ViewName, NomeCadastro, 
            FieldNames, ShowFieldNames) na contrução da classe herdada:
                * Database: Nome da database;
                * TableName: Nome tabela;
                * ViewName: Nome visão do cadastro, caso possuir. 
                    Se não possui passar "". A visão possui egras no nome dos 
                    campos, descrito nas regras do BD;
                * NomeCadastro: Nome do cadastro apenas para mostrar na tela e
                    relatório. Se o cadastros for de clientes, passar apeans "Clientes";
                * FieldNames: Nomes dos campos da tabelas ou visão que serão mostrados 
                    no grid dos dados. O primeiro deve ser exatamente o código da 
                    tabela ou visão. Passar por vetor de Strings. Ex:
                    new String[] {"CodCliente", "Nome"}
                * ShowFieldNames: Nome que será mostrado nas colunas da tabela 
                    representando cada FieldName. Deve ser informado na mesma 
                    ordem do FieldName. Ex: new String[] {"Código", "Nome"};       
        6 - Adicionar os componetes de entrada (JTextComponents  e Combobox) na 
            tela correspondendo a cada campo da tabela. Na propriedade Name de cada
            componente deve ser inserido o nome do campo que corresponde na tabela;
        7 - Deve-se ajustar a tela em função dos componentes, pois a mesma irá 
            adaptar os botões sobre a mesma;
        8 - Se for necessário atualziar um combobox após um inserção, chamar o método
            LoadComboBox() após;
        9 - Quando for abrir o cadastro não deve chamar o método setVisible(true),
            pois é feito internamente. Apenas instaciar, ex: new CaCliente();

       REGRAS DO BD

        1 - Todas as tabelas devem ter como chave primária o prefixo "Cod" mais, 
            o nome da tabela. Se a tabela chamar cliente, a PK deve ser CodCliente.
            Caso contrário irá apresentar erros;
        2 - Se for necessário mostrar dados de uma tabela no ComboBox, o campo
            que irá mostrar deve chamar "Nome".

 */
package CadastroBD;
import BD.ConexaoMYSQL;
import java.awt.Component;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Windows
 */
public class paCadastroEntradas extends javax.swing.JDialog {
    
    protected int Codigo;
    protected String TableName;
    private String View;
    private String Titulo;
    protected ConexaoMYSQL c;//AQUI
    protected boolean IsNovoRegistro;
    private ArrayList<JComponent> Componentes;
    private ArrayList<String> FieldNames;
    private ArrayList<Integer> FieldTypes;
    private paCadastroMenu cadastroMenu;
    
    public paCadastroEntradas() {
        this.setModal(true);//impedi abertura de outra tela
        this.Componentes = new ArrayList<JComponent>();
        this.FieldNames = new ArrayList<String>();
        this.FieldTypes = new ArrayList<Integer>();
    }
    
    
    protected void Load(String Database, String Tabela, String view, String Titulo, String FiltroMenu, 
            String[] Columns, String[] NomeColunas){                     

        int Altura = this.getSize().height + 40;//Pegando altura e largura antes de iniar os componentes.
        int Largura = this.getSize().width;

        initComponents();//inicializa os componentes da tela no caso os botes salver e cancelar.
        this.setSize(Largura, Altura);  
        
        this.c = new ConexaoMYSQL(Database);//tenho que instanciar a conexao antes do começar os metodos que precise dela.
        this.TableName = Tabela;
        this.View = view;
        this.Titulo = Titulo;
        this.getComponentes(this.getComponents());//Passo como parametros os proprios componentes.
        
        //Abrir a tela do menu   
        this.cadastroMenu = new paCadastroMenu(this.c, Database, Tabela, view, Titulo, FiltroMenu, Columns, NomeColunas,
                this);
        //Por ultimo eu passo simplismente um this q é a propria tela.                                 
    }
    
    //ESSE LOAD É PARA LIDAR COM TRANSAÇÕES 
    protected void Load(ConexaoMYSQL c, String Database, String Tabela, String view, String Titulo, 
            String FiltroMenu, String[] Columns, String[] NomeColunas){                     
        int Altura = this.getSize().height + 40;
        int Largura = this.getSize().width;
        initComponents();
        this.setSize(Largura, Altura);  
        
        this.c = c;//O de cima dá um NEW em uma nova conexao esse daqui eu passo a mesma conexao como parametro.
        this.TableName = Tabela;
        this.View = view;
        this.Titulo = Titulo;
        this.getComponentes(this.getComponents());//Passo como parametros os proprios componentes.
        
        //Abrir a tela do menu   
        this.cadastroMenu = new paCadastroMenu(this.c, Database, Tabela, view, Titulo, FiltroMenu, 
                Columns, NomeColunas, this);                              
    }
    
    //Esse Load serve para Ocultar algum botao lá no cadastro Menu
    protected void Load(String Database, String Tabela, String view, String Titulo, String FiltroMenu, 
            String[] Columns, String[] NomeColunas, String OcultarBotao){                     

        int Altura = this.getSize().height + 40;//Pegando altura e largura antes de iniar os componentes.
        int Largura = this.getSize().width;

        initComponents();//inicializa os componentes da tela no caso os botes salver e cancelar.
        this.setSize(Largura, Altura);  
        
        this.c = new ConexaoMYSQL(Database);//tenho que instanciar a conexao antes do começar os metodos que precise dela.
        this.TableName = Tabela;
        this.View = view;
        this.Titulo = Titulo;
        this.getComponentes(this.getComponents());//Passo como parametros os proprios componentes.
        
        //Abrir a tela do menu   
        this.cadastroMenu = new paCadastroMenu(this.c, Database, Tabela, view, Titulo, FiltroMenu, Columns, NomeColunas,
                this, OcultarBotao);
        //Por ultimo eu passo simplismente um this q é a propria tela.                                 
    }
    
    public void AbrirMenu(){
            this.cadastroMenu.setVisible(true);
    }
    
    
    protected void CarregarComboBox(JComboBox ComboBox, String Table){
        this.c.setComboBox(ComboBox, Table);
    }
    
    protected void Abrir(int Codigo){
        this.Codigo = Codigo;
        this.IsNovoRegistro = (this.Codigo == 0);//Se for um cadastro e o codigo for zero a variavel recebe um
                //true se for uma edição recebe um false;
        
        //Caso possuir combobox carregar todos
        for(int i=0; i<this.Componentes.size(); i++) {
            
            if (this.Componentes.get(i) instanceof JComboBox) {
                JComboBox CB = (JComboBox) this.Componentes.get(i);
                if(CB.getName().equalsIgnoreCase("Cod"+CB.getName().substring(3))){
                    String TabelaFK = CB.getName().substring(3);
                    this.CarregarComboBox(CB, TabelaFK);
                }
                else{
                    //Se o combobox não tiver chave estrangeira ñ faz nada.
                }      
            }
        }
        
        if(this.Codigo == 0){
            this.setTitle("Cadastro de " +this.Titulo);
            
            //resetar componenetes
            for(int i=0; i<this.Componentes.size(); i++) {
                //limpar campos texts
                if (this.Componentes.get(i) instanceof JTextComponent) {
                    JTextComponent TextComponent = (JTextComponent) this.Componentes.get(i); 
                    if(TextComponent.isVisible())
                        TextComponent.setText("");
                } 
                //definir combobox sem seleção
                else if (this.Componentes.get(i) instanceof JComboBox) {
                    JComboBox CB = (JComboBox) this.Componentes.get(i);
                    CB.setSelectedIndex(-1);
                }                
            }
        }
        else{
            this.setTitle("Editar " +this.Titulo);
                    
            try {
                if(this.View.isEmpty()){
                    this.c.setResultSet("SELECT * FROM " + this.TableName + " WHERE Cod"+this.TableName+ " = " +
                        Codigo);
                }
                else{
                    this.c.setResultSet("SELECT * FROM " + this.View + " WHERE Cod"+this.TableName+ " = " +
                        Codigo);//Mandando a view como consulta
                }
                this.c.getResultSet().first();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
            
            //Carregando os campos
            for(int i=0; i<this.Componentes.size(); i++){
                
                //Carregar campos de texto
                if(this.Componentes.get(i) instanceof JTextComponent){
                    JTextComponent TextComponent = (JTextComponent) this.Componentes.get(i);
                    String ValorCampo = null;
                    
                    if(TextComponent.isVisible()){
                        try {
                            ValorCampo = this.c.getResultSet().getString(TextComponent.getName());
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                    } 
                    
                    //Se o campo for do tipo data, definir padrão brasileiro
                    if (this.FieldTypes.get(i) == Types.DATE || this.FieldTypes.get(i) == Types.TIMESTAMP) {
                        Date data = null;
                        try {
                            data = c.getResultSet().getDate(this.FieldNames.get(i));
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                        ValorCampo = formato.format(data);
                    }
                    TextComponent.setText(ValorCampo);
                }
                //Carregando o combobox
                else if(this.Componentes.get(i) instanceof JComboBox){
                          
                    try {
                        JComboBox cb =  (JComboBox) this.Componentes.get(i);//Aqui tem é que selecionar um!!
                        cb.setSelectedItem(this.c.getResultSet().getString(cb.getName()));
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }
            }
        }
        this.AbrirAddFuncionalidades();//Estre método tem a finalidade de adicionar funcionalidades adicionais.
        this.AddFuncionalidadesTransacao();//Este metodo vai startar a transação.
        this.setVisible(true);//Para deixar o cadastro visivel!!!
    }
    
    
    private void LimparComponentes(Component[] Componentes){
        for(int i=0; i<Componentes.length; i++){
        //mostrar o nome do componente   JOptionPane.showMessageDialog(this, Componentes[i].getName());
            if(Componentes[i] instanceof JTextComponent){
                JTextComponent tc = (JTextComponent) Componentes[i];
                if(tc.isVisible())
                    tc.setText("");
            }
            else if(Componentes[i] instanceof JComboBox){
                JComboBox cb = (JComboBox) Componentes[i];
                cb.setSelectedIndex(-1);
            }
            else{
                JComponent comp = (JComponent) Componentes[i];
                if(comp.getComponentCount() > 0){
                    LimparComponentes(comp.getComponents());
                }
            }
        }
    }
    
    
    //Método para buscar todos os componentes na tela
    private void getComponentes(Component[] Componentes) {
        
        for(int i=0; i<Componentes.length; i++) {   
            
            JComponent Component = (JComponent) Componentes[i];

            //TextComponents e combo box
            if (Componentes[i] instanceof JTextComponent || Componentes[i] instanceof JComboBox ) {
                
                if(!Component.getName().isEmpty()){//SE O COMPONENTE NAO TIVER NOME ELE N SERA ADICIONADO!!
                    this.Componentes.add(Component);//Aqui eu pego o componente para o array de componentes!
                    this.FieldNames.add(Component.getName());//Aqui eu pego o 'NOME' do componente!!!
                    //add tipo do campo
                    try {
                        c.setResultSet("SELECT " + Component.getName() + " FROM " + this.TableName);                                                       
                        ResultSetMetaData rsmd = c.getResultSet().getMetaData();
                        this.FieldTypes.add(rsmd.getColumnType(1));                    
                    }
                    catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, e);
                    } 
                }                              
            }
            else {                                
                //Existem componentes que não possuem a propriedade getComponentCount()
                try {
                    if (Component.getComponentCount() > 0) {
                      getComponentes(Component.getComponents());   
                    }
                }
                catch (Exception e) {
                    
                }
            }
            
        }//Fim for       
    }
    
    //Este método tem finalidade de ser sobreescrito para caso deseja validar as entradas
    protected boolean Validar(){
        return true;
    }
    
    
    //Estre método tem a finalidade de adicionar funcionalidades adicionais 
    //  quando for abrir as entradas de dados
    protected void AbrirAddFuncionalidades() {
        
    }
    
    
    protected void Salvar(){
        //Salvar
        //O método sobrescrito do paCad01Entrada é executado. 
        if(this.Validar()){//Se for vdd ou seja está preenchido então entra.
            
            if(this.Codigo == 0){
                String SQL = "INSERT INTO "+ this.TableName+ " (";

                //CARREGANDO OS CAMPOS
                for(int i=0; i<this.FieldNames.size(); i++){//Primeiros sao os campos
                    SQL = SQL + this.FieldNames.get(i);

                    if(i < this.FieldNames.size() - 1)
                        SQL = SQL + ", ";
                }
                SQL = SQL + ") VALUES (";

                
                //CARREGANDO OS VALORES DOS CAMPOS
                for(int i=0; i<this.Componentes.size(); i++){
                    if(this.Componentes.get(i) instanceof JTextComponent){//Se tiver instanciado na tela.
                        JTextComponent tc = (JTextComponent) this.Componentes.get(i);  
                        String ValorCampo = tc.getText(); 
                        
                        //Se o campo for do tipo data, definir o padrão do banco
                        if (this.FieldTypes.get(i) == Types.DATE || this.FieldTypes.get(i) == Types.TIMESTAMP) {
                            SQL = SQL + "'" + this.InverterData(i, ValorCampo) + "'";                        
                        }
                        else
                            SQL = SQL + "'" + ValorCampo + "'";
                    }
                    if(this.Componentes.get(i) instanceof JComboBox){//Se tiver instanciado na tela.
                        JComboBox cb =  (JComboBox) this.Componentes.get(i);
                        
                        if(cb.getName().equalsIgnoreCase("Cod"+cb.getName().substring(3)))
                            SQL = SQL + " '"+this.c.ObterCodigo(cb)+"' ";
                        else
                            SQL = SQL + " '"+cb.getSelectedItem()+"' ";
                    }
                    
                    if(i < this.FieldNames.size() - 1)
                        SQL = SQL + ", ";
                }
                SQL = SQL + ");";

                if(this.c.SQLExecute(SQL)){//Se não der erro entra!
                    JOptionPane.showMessageDialog(null, "Registro cadastrado com sucesso!");
                    //JOptionPane.showMessageDialog(null, SQL);
                    //Aqui nao precisa do this.SalvarFuncionalidadesTransacao pq o this.codigo pega o ultimo codigo
                    //fazendo com que seja tratado como uma edição.
                    this.dispose();
                }
            }
            //Editar
            else{
                String SQL = "UPDATE " +this.TableName+ " SET ";
                
                for(int i=0; i<this.Componentes.size(); i++){
                    if(this.Componentes.get(i) instanceof JTextComponent){//Se tiver instanciado na tela.
                        JTextComponent tc =  (JTextComponent) this.Componentes.get(i);
                        String ValorCampo = tc.getText();
                        
                        //Se o campo for do tipo data, definir o padrão do banco
                        if (this.FieldTypes.get(i) == Types.DATE || this.FieldTypes.get(i) == Types.TIMESTAMP) {
                            SQL = SQL + tc.getName() + " = '"+ this.InverterData(i, ValorCampo) +"' ";                    
                        }
                        else
                            SQL = SQL + tc.getName() + " = '"+ ValorCampo +"' ";
                    }
                    if(this.Componentes.get(i) instanceof JComboBox){//Se tiver instanciado na tela.
                        JComboBox cb =  (JComboBox) this.Componentes.get(i);
                        
                        if(cb.getName().equalsIgnoreCase("Cod"+cb.getName().substring(3)))
                            SQL = SQL +  cb.getName() + " = '"+this.c.ObterCodigo(cb)+"' ";
                        else
                            SQL = SQL +  cb.getName() + " = '"+cb.getSelectedItem()+"' ";
                    }
                    
                    if(i < this.FieldNames.size() - 1)
                        SQL = SQL + ", ";
                }              
                SQL = SQL + " WHERE Cod" + this.TableName + " = " + String.valueOf(this.Codigo) + ";";
                
                if(this.c.SQLExecute(SQL)){//Se não der erro entra!
                    this.SalvarFuncionalidadesTransacao();//Tirei a msg daqui pq eu posso ter um cadastro ai
                        //tenho q mostrar a msg de cadastro e quando for um update mostra a msg correspondente.
                    this.dispose();
                }
            }
            
        }//Fim if  
    }
    
    protected void AddFuncionalidadesTransacao() {
        /*Quando uma classe herda de paCadastroEntradas esse método é chamado e simplismente não faz nada, pq
            não tem nada implementado nesse método!.
          Mas quando uma classe herda de paCadastroEntradasTransacao esse método é sobreescrito e é chamado e 
        tudo que está implementado dentro dele é executado ou seja ele inicia a transação*/
    }
    
    //Se não for herdado de paCadastroEntradasTransacao vai só mostrar uma das mensagens.
    protected void SalvarFuncionalidadesTransacao() {
        JOptionPane.showMessageDialog(null, "Registro editado com sucesso!");
    }
    
    protected void Cancelar(){
        String texto;
        if(this.Codigo == 0)
            texto = "Se confirmar a inserção será perdida!";
        else
            texto = "Se confirmar todas as modificações serão perdidas!";
        
        if(JOptionPane.showConfirmDialog(null, texto + "\nDeseja realmente cancelar?", "Cancelar",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
            this.dispose();
        }
    }
    
    
    private String InverterData(int Posicao, String ValorCampo){
   
        //Se o campo for do tipo data, definir o padrão do banco
        if (this.FieldTypes.get(Posicao) == Types.DATE || this.FieldTypes.get(Posicao) == Types.TIMESTAMP) {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date data = formato.parse(ValorCampo);
                formato.applyPattern("yyyy-MM-dd");
                ValorCampo = formato.format(data);
            } catch (ParseException ex) {
               JOptionPane.showMessageDialog(null, ex);
            }                            
        }

        return ValorCampo;
    }
    
    

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(170, Short.MAX_VALUE)
                .addComponent(btnSalvar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancelar)
                .addGap(6, 6, 6))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar))
                .addGap(0, 7, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 176, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        // TODO add your handling code here: 
        this.Salvar();
    }//GEN-LAST:event_btnSalvarActionPerformed
    
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.Cancelar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        this.Cancelar();
    }//GEN-LAST:event_formWindowClosing



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
