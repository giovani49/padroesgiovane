/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CadastroBD;

import PadroesTela.Grid;
import PadroesTela.Validacoes;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Windows
 */
public class paFiltro extends javax.swing.JDialog {
    
    private int[] FieldTypes;
    private String[] FieldShowNames;
    private String[] FieldNames;
    private ArrayList<Integer> Tipotual;
    private ArrayList<String> CampoAtual;
    private ArrayList<String> CriterioAtual;
    private ArrayList<String> ValorAtual;
    private ArrayList<Integer> IndiceAtual;
    private String SQLFiltro;
    
    public paFiltro(int[] FieldTypes, String[] FieldNames, String[] FieldShowNames) {
        initComponents();
        this.setModal(true); 
        
        this.FieldTypes = FieldTypes;
        this.FieldShowNames = FieldShowNames;
        this.FieldNames = FieldNames;
        this.SQLFiltro = "";
        
        this.Tipotual = new ArrayList<Integer>();//Instanciando as variaveis.
        this.CampoAtual = new ArrayList<String>();
        this.CriterioAtual = new ArrayList<String>();
        this.ValorAtual = new ArrayList<String>();  
        this.IndiceAtual = new ArrayList<Integer>();
        
        //preencher lista de campos
        cbCampo.removeAllItems();
        for(int i=0; i<FieldShowNames.length; i++)
          cbCampo.addItem(FieldShowNames[i]);
        
        //criar o model do grid
        //DefaultTableModel dtm = new DefaultTableModel();
        //dtm = (DefaultTableModel) Grid.getModel();
    }
    
    
    public void Abrir() {
        //A cada vez que eu abrir a tela vai limpar e preemcher com o que tiver no grid tem que fazer assim
        //Copiar do grid as colunas para os arrays
        Tipotual.clear();
        CampoAtual.clear();
        CriterioAtual.clear();
        ValorAtual.clear();
        IndiceAtual.clear();
        
        for(int i=0; i<Grid.getRowCount(); i++) {
            Tipotual.add(Integer.parseInt(String.valueOf(Grid.getValueAt(i, 0))));
            CampoAtual.add(String.valueOf(Grid.getValueAt(i, 1)));
            CriterioAtual.add(String.valueOf(Grid.getValueAt(i, 2)));
            ValorAtual.add(String.valueOf(Grid.getValueAt(i, 3)));
            IndiceAtual.add(Integer.parseInt(String.valueOf(Grid.getValueAt(i, 4))));
        }
        
        this.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cbCampo = new javax.swing.JComboBox<>();
        cbCampoComparacao = new javax.swing.JComboBox<>();
        lblCampoComparacao = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        lblValor = new javax.swing.JLabel();
        btnInserir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Grid = new javax.swing.JTable();
        btnExcluir = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnConfirmar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Campo");

        cbCampo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbCampoItemStateChanged(evt);
            }
        });

        lblCampoComparacao.setText("Critério de comparação");

        lblValor.setText("Valor");

        btnInserir.setText("Inserir");
        btnInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInserirActionPerformed(evt);
            }
        });

        Grid.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipo", "Campo", "Critério", "Valor", "Indice"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(Grid);
        if (Grid.getColumnModel().getColumnCount() > 0) {
            Grid.getColumnModel().getColumn(0).setMinWidth(0);
            Grid.getColumnModel().getColumn(0).setPreferredWidth(0);
            Grid.getColumnModel().getColumn(0).setMaxWidth(0);
            Grid.getColumnModel().getColumn(1).setMinWidth(170);
            Grid.getColumnModel().getColumn(1).setPreferredWidth(170);
            Grid.getColumnModel().getColumn(1).setMaxWidth(170);
            Grid.getColumnModel().getColumn(2).setMinWidth(120);
            Grid.getColumnModel().getColumn(2).setPreferredWidth(120);
            Grid.getColumnModel().getColumn(2).setMaxWidth(120);
            Grid.getColumnModel().getColumn(4).setMinWidth(0);
            Grid.getColumnModel().getColumn(4).setPreferredWidth(0);
            Grid.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 562, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(0, 141, Short.MAX_VALUE))
                                    .addComponent(cbCampo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbCampoComparacao, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblCampoComparacao))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnInserir))
                                    .addComponent(lblValor))))
                        .addGap(11, 11, 11))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnExcluir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConfirmar)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCampoComparacao)
                    .addComponent(jLabel1)
                    .addComponent(lblValor))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbCampo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbCampoComparacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnInserir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnExcluir)
                    .addComponent(btnEditar)
                    .addComponent(btnCancelar)
                    .addComponent(btnConfirmar))
                .addGap(10, 10, 10))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.LimpandoGrid();
        for(int i=0; i<Tipotual.size(); i++) {//Esse método é executado com base no tamanho dos Arrays.
            this.PreencherGridCriterios(Grid, Tipotual.get(i),CampoAtual.get(i), CriterioAtual.get(i),
                                            ValorAtual.get(i), IndiceAtual.get(i));
        }
        
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void cbCampoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbCampoItemStateChanged
        // TODO add your handling code here:
        int CampoSelecionado = cbCampo.getSelectedIndex();
        
        if(this.FieldTypes[CampoSelecionado]  == Types.DATE || this.FieldTypes[CampoSelecionado] == Types.REAL ||
                this.FieldTypes[CampoSelecionado] == Types.INTEGER || this.FieldTypes[CampoSelecionado] == Types.FLOAT ||
                this.FieldTypes[CampoSelecionado] == Types.DOUBLE || this.FieldTypes[CampoSelecionado] == Types.BIGINT){ 
            //Preenchendo o campo comparaçao      //Cada tipo é representado por um numero.
            cbCampoComparacao.removeAllItems();
            cbCampoComparacao.addItem("Maior");
            cbCampoComparacao.addItem("Maior ou Igual");
            cbCampoComparacao.addItem("Menor");
            cbCampoComparacao.addItem("Menor ou Igual");
            cbCampoComparacao.addItem("Igual");
            cbCampoComparacao.addItem("Diferente");
        }
        else{
            //Preenchendo o campo comparaçao 
            cbCampoComparacao.removeAllItems();
            cbCampoComparacao.addItem("Contém");
            cbCampoComparacao.addItem("Não Contém");
            cbCampoComparacao.addItem("Igual");
            cbCampoComparacao.addItem("Diferente");
            cbCampoComparacao.addItem("Inicia com");
            cbCampoComparacao.addItem("Termina com");
        }
    }//GEN-LAST:event_cbCampoItemStateChanged

    private void btnInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInserirActionPerformed
        // TODO add your handling code here:
        Validacoes v = new Validacoes();
        int Posicao = cbCampo.getSelectedIndex();
        boolean AtendCrt = false; //Atende criterio
        
        if(v.IsFilled(txtValor, "Valor")){
            
            if(this.FieldTypes[Posicao] == Types.REAL || this.FieldTypes[Posicao] == Types.FLOAT ||
                this.FieldTypes[Posicao] == Types.DOUBLE ){
                
                if(v.IsFloat(txtValor, "Valor")){
                    AtendCrt = true;
                }
            }
            
            else if(this.FieldTypes[Posicao] == Types.INTEGER || this.FieldTypes[Posicao] == Types.BIGINT){
                
                if(v.IsInteger(txtValor, "Valor")){
                    AtendCrt = true;
                }
            }
            
            else if(this.FieldTypes[Posicao]  == Types.DATE){
                
                if(v.IsDate(txtValor, "Valor")){
                    AtendCrt = true;
                }
            }
            
            else{
                AtendCrt = true;
            }
            
            if(AtendCrt){
                this.PreencherGridCriterios(Grid, this.FieldTypes[Posicao], 
                                                String.valueOf(cbCampo.getSelectedItem()), 
                                                String.valueOf(cbCampoComparacao.getSelectedItem()),
                                                txtValor.getText(), Posicao);
                txtValor.setText("");
            }
        }
    }//GEN-LAST:event_btnInserirActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        // TODO add your handling code here:
        DefaultTableModel dtm = new DefaultTableModel();
        dtm = (DefaultTableModel) Grid.getModel();
        
        if (Grid.getRowCount() < 1)
            JOptionPane.showMessageDialog(null, "O cadastro está vazio");
        else if (Grid.getSelectedRowCount() > 0) { //testar se existe alguma linha selecionada
            //confirmação se usuário deseja realmente excluir                                   
            if (JOptionPane.showConfirmDialog(null, "Confirmar exclusão?", 
                    "Exclusão de dados", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {            

                dtm.removeRow(Grid.getSelectedRow());
                JOptionPane.showMessageDialog(null, "Registro excluído com sucesso!");
            }            
        }    
        else
            JOptionPane.showMessageDialog(null, "Selecionar um registro");
    }//GEN-LAST:event_btnExcluirActionPerformed

    
    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed

        this.GetFiltro();
        this.dispose();
    }//GEN-LAST:event_btnConfirmarActionPerformed
    
    
    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
        ArrayList vlEdt = new ArrayList();
        //Passando a primeira coluna escondida do grid que tem o tipo do valor
        String ValorTipo = String.valueOf(Grid.getValueAt(Grid.getSelectedRow(), 0));
        int Tipo = Integer.parseInt(ValorTipo);
        
        if (Grid.getRowCount() < 1)
            JOptionPane.showMessageDialog(null, "O cadastro está vazio.");
        else if (Grid.getSelectedRowCount() > 0) { //testar se existe alguma linha selecionada
            new paFiltroEditar(Tipo, Grid.getValueAt(Grid.getSelectedRow(), 2), 
                    Grid.getValueAt(Grid.getSelectedRow(), 3), vlEdt).setVisible(true);
            if(!vlEdt.isEmpty()){
                Grid.setValueAt(vlEdt.get(0), Grid.getSelectedRow(), 2);
                Grid.setValueAt(vlEdt.get(1), Grid.getSelectedRow(), 3);
                JOptionPane.showMessageDialog(null, "Registro atualizado com sucesso!");
            }
        }  
        else{
            JOptionPane.showMessageDialog(null, "Selecionar um registro.");
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    
    private void LimpandoGrid(){
        DefaultTableModel dtm = new DefaultTableModel();
        dtm = (DefaultTableModel) Grid.getModel();
        
        //Limpar o grid e preencher com os arraylist    
        //Excluir todas as linhas
        while(dtm.getRowCount() > 0) {
          dtm.removeRow(0);             
        } 
    }
    
    private void PreencherGridCriterios(JTable Grid, int Tipo, String Campo, String Criterio, String Valor, int Indice){
        DefaultTableModel dtm = new DefaultTableModel();
        dtm = (DefaultTableModel) Grid.getModel();
        
        Object linha[] = new Object[Grid.getColumnCount()];
        for(int i=0; i<Grid.getColumnCount(); i++){
            if(i == 0)
               linha[i] = Tipo;
            else if(i == 1)
                linha[i] = Campo;
            else if(i == 2)
                linha[i] = Criterio;
            else if(i == 3)
                linha[i] = Valor;
            else
                linha[i] = Indice;
        }
        dtm.addRow(linha);
    }
    
    
    public String GetFiltro(){
        //Caça todos os campos e criterios iguais para o botar o (OR ||).
        //Equanto for falsa a linha vai ser analizada. boolean LinhasAdd[]
        this.SQLFiltro = "";// False com False dá True
        
        if(Grid.getRowCount() > 0){
            //Definir primeiramente linhas nao selecionadas ou seja com false.
            boolean LinhasAdd[] = new boolean[Grid.getRowCount()];
            
            for(int i=0; i<LinhasAdd.length; i++){
                LinhasAdd[i] = false;
            }
                
            //percorrer cada linha do Grid
            for(int i=0; i<Grid.getRowCount(); i++){
                
                if(!LinhasAdd[i]){//Se a linha estiver falsa entra
                    SQLFiltro = SQLFiltro + "(";
                    SQLFiltro = SQLFiltro + FieldNameCorrespondente(Grid.getValueAt(i, 4)) + " " +
                                    GerarSintaxe(Integer.parseInt(String.valueOf(Grid.getValueAt(i, 4))),
                                        Grid.getValueAt(i, 2), String.valueOf(Grid.getValueAt(i, 3)));
                    //Nesse método primeiro eu passo o (Tipo) depois o (criterio) e o (valor).
                    
                    //Localizar se existem campos com os criterios iguais.
                    for(int j=i+1; j<Grid.getRowCount(); j++){
                        if(!LinhasAdd[j] && Grid.getValueAt(i, 1).equals(Grid.getValueAt(j, 1)) &&
                            Grid.getValueAt(i, 2).equals(Grid.getValueAt(j, 2)) ){
                        //Se o valor da linha campo estiver igual ao valor da linha campo abaixo &&
                        //e o valor da linha criterio estiver igual ao valor da  linha criterio abaixo bota (OR)
                            SQLFiltro = SQLFiltro + " OR " + FieldNameCorrespondente(Grid.getValueAt(j, 4)) +
                                        GerarSintaxe(Integer.parseInt(String.valueOf(Grid.getValueAt(j, 4))),
                                         Grid.getValueAt(j, 2), String.valueOf(Grid.getValueAt(j, 3)));
                            LinhasAdd[j] = true;
                        }
                    }
                    LinhasAdd[i] = true;
                    SQLFiltro = SQLFiltro + ")";
                    
                    if(i < Grid.getRowCount()-1){ //Coloca o -1 pra nao botar o AND na ultima linha
                        for(int j=i+1; j<Grid.getRowCount(); j++){
                            if(!LinhasAdd[j]){//Se a linha estiver falsa bota o AND se tiver true ñ bota.
                                SQLFiltro = SQLFiltro + " AND ";
                                break;
                            }
                        }
                    }//Esse If está dentro 
                }//Fim IF  
                
            }//Fim For
            return SQLFiltro;
        }//Fim IF
        else
            return "";
    }
    
    
    private String FieldNameCorrespondente(Object Posicao){
        String FieldName = this.FieldNames[Integer.parseInt(String.valueOf(Posicao))];
        
        return FieldName;
    }
    
    
    private String GerarSintaxe(int Position, Object Criterio, String Valor){
        int Posicao = Position;
        String Simbolo = null;
        String[] SimbStrings = {"Contém", "Não Contém", "Igual", "Diferente", "Inicia com", "Termina com"};
        String[] SimReais = {"Maior", "Maior ou Igual", "Menor", "Menor ou Igual", "Igual", "Diferente"};
        
        if(this.FieldTypes[Posicao] == Types.DOUBLE || this.FieldTypes[Posicao] == Types.REAL || 
                this.FieldTypes[Posicao] == Types.FLOAT || this.FieldTypes[Posicao] == Types.INTEGER || 
                this.FieldTypes[Posicao] == Types.BIGINT){
            
            if(SimReais[0].equals(Criterio))
                Simbolo = " > " + " '"+Valor+"' ";
            else if(SimReais[1].equals(Criterio))
                Simbolo = " >= " + " '"+Valor+"' ";
            else if(SimReais[2].equals(Criterio))
                Simbolo = " < " + " '"+Valor+"' ";
            else if(SimReais[3].equals(Criterio))
                Simbolo = " <= " + " '"+Valor+"' ";
            else if(SimReais[4].equals(Criterio))
                Simbolo = " = " + " '"+Valor+"' ";
            else
                Simbolo = " != " + " '"+Valor+"' ";

        }
        else if(this.FieldTypes[Posicao] == Types.DATE){
            
            if(SimReais[0].equals(Criterio))
                Simbolo = " > " + " '"+this.InverterData(Valor)+"' ";
            else if(SimReais[1].equals(Criterio))
                Simbolo = " >= " + " '"+this.InverterData(Valor)+"' ";
            else if(SimReais[2].equals(Criterio))
                Simbolo = " < " + " '"+this.InverterData(Valor)+"' ";
            else if(SimReais[3].equals(Criterio))
                Simbolo = " <= " + " '"+this.InverterData(Valor)+"' ";
            else if(SimReais[4].equals(Criterio))
                Simbolo = " = " + " '"+this.InverterData(Valor)+"' ";
            else
                Simbolo = " != " + " '"+this.InverterData(Valor)+"' ";
        }
        else{
            
            if(SimbStrings[0].equals(Criterio))
                Simbolo =  " LIKE  '%"+Valor+"%' ";
            else if(SimbStrings[1].equals(Criterio))
                Simbolo = " NOT LIKE " + " '%"+Valor+"%' ";
            else if(SimbStrings[2].equals(Criterio))
                Simbolo = " LIKE " + " '"+Valor+"' ";
            else if(SimbStrings[3].equals(Criterio))
                Simbolo = " <> " + " '"+Valor+"' ";
            else if(SimbStrings[4].equals(Criterio))
                Simbolo = " LIKE  '%"+Valor+"' ";
            else
                Simbolo = " LIKE  '"+Valor+"%' ";
        }    
        return Simbolo;
    }
    
    
   private String InverterData(String ValorCampo){
        //Se o campo for do tipo data, definir o padrão do banco
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date data = formato.parse(ValorCampo);
            formato.applyPattern("yyyy-MM-dd");
            ValorCampo = formato.format(data);
        } catch (ParseException ex) {
           JOptionPane.showMessageDialog(null, ex);
        }                            

        return ValorCampo;
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Grid;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnInserir;
    private javax.swing.JComboBox<String> cbCampo;
    private javax.swing.JComboBox<String> cbCampoComparacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCampoComparacao;
    private javax.swing.JLabel lblValor;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
