/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PadroesTela;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author henriquefari
 */
public class Grid {
    
    
    public void setDelete(JTable Grid, ArrayList arrayList) {
        //verificar se o grid possui registros
        if (Grid.getRowCount() < 1)
            JOptionPane.showMessageDialog(null, "O cadastro está vazio.");
        else if (Grid.getSelectedRowCount() > 0) { //testar se existe alguma linha selecionada
            //confirmação se usuário deseja realmente excluir                                   
            if (JOptionPane.showConfirmDialog(null, "Confirmar exclusão?", 
                    "Exclusão de dados", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {            
                //Vetor para alocar as linhas selecionadas
                int[] Linhas = Grid.getSelectedRows();//Linhas recebe a qtd de linhas selecionadas.
                //Total de itens excluídos confirmados
                int TotalConfirmados = 0;
                for (int i=Linhas.length-1; i>=0; i--) {                                       
                    arrayList.remove(Linhas[i]);
                    TotalConfirmados++;
                }
                if (TotalConfirmados == Linhas.length)
                    JOptionPane.showMessageDialog(null, "Registro(s) excluído(s) com sucesso.");                                                                         
            }            
        }    
        else
            JOptionPane.showMessageDialog(null, "Selecionar um registro.");
    }
    
    
    public String getValor(JTable Grid, int Coluna) {
        
        String Valor = "";
        //Verificando se o grid possui registros!
        if (Grid.getRowCount() < 1)
            JOptionPane.showMessageDialog(null, "O cadastro está vazio.");
        else if (Grid.getSelectedRowCount() > 0) { //testar se existe alguma linha selecionada   
            if (Grid.getSelectedRowCount() > 1) {
                JOptionPane.showMessageDialog(null, "Selecionar apenas um registro.");
            }
            else {
                int Linha = Grid.getSelectedRow();
                Valor = String.valueOf(Grid.getValueAt(Linha, Coluna));   
                //O segundo parametro é a coluna já que o meu código sempre vai ficar na coluna Zero.
            }         
        }    
        else
            JOptionPane.showMessageDialog(null, "Selecionar um registro.");
        
        return Valor; 
    }
    
    
    public String getValor02(JTable Grid, int Coluna) { 
        String Valor = "";
        //Verificando se o grid possui registros!

        int Linha = Grid.getSelectedRow();
        Valor = String.valueOf(Grid.getValueAt(Linha, Coluna));   
        //O segundo parametro é a coluna já que o meu código sempre vai ficar na coluna Zero.
        
        return Valor; 
    }
    
   
    public void setGrid(JTable Grid, String[] FieldShowNames) {
        //Definir que colunas não podem ser editadas
        boolean[] canEdit = new boolean[FieldShowNames.length];
        for(int i=0; i<FieldShowNames.length; i++) {
            canEdit[i] = false;
        }
        
        //Definir tipos das colunas (todas objects)
        Class[] types = new Class[FieldShowNames.length];
        for(int i=0; i<FieldShowNames.length; i++) {
            types[i] = java.lang.Object.class;
        }
        
        Grid.setModel(new javax.swing.table.DefaultTableModel(
            //Linhas    
            new Object [][] {},
            //Colunas    
            FieldShowNames) 
        {

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });     
        
        //definir todas com tamanho minimo zero
        for(int i=0; i<FieldShowNames.length; i++) {
            Grid.getColumnModel().getColumn(i).setMinWidth(0);
        }
    }
}
