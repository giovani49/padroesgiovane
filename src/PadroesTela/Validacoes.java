/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PadroesTela;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author henriquefari
 */
public class Validacoes {
    
    
    public boolean IsFloat(JTextField TextField, String NomeCampo) {        
        try {
            Double.parseDouble(TextField.getText());
            return true;//Se der certo retorna TRUE.
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Valor incorreto do campo: " + NomeCampo, 
                    "Valor incorreto", JOptionPane.ERROR_MESSAGE);                        
            TextField.setText("");
            TextField.requestFocus();
            return false;//Se der errado retorna FALSE.
        }                
    }
    
    
    public boolean IsInteger(JTextField TextField, String NomeCampo) {        
        try {
            Integer.parseInt(TextField.getText());
            return true;//Se der certo retorna TRUE.
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Valor incorreto do campo: " + NomeCampo, 
                    "Valor incorreto", JOptionPane.ERROR_MESSAGE);                        
            TextField.setText("");
            TextField.requestFocus();
            return false;//Se der errado retorna FALSE.
        }                
    }
    
    
    public boolean IsDate(JTextField TextField, String NomeCampo) {        
        try {
            DateTimeFormatter formatoData = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            
            LocalDate.parse(TextField.getText(), formatoData);
            return true;//Se der certo retorna TRUE.
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Preencher o campo com: DD/MM/AAAA", 
                    "Campo não preenchido da forma correta", JOptionPane.ERROR_MESSAGE);                        
            TextField.setText("");
            TextField.requestFocus();
            return false;//Se der errado retorna FALSE.
        }                
    }
    
    
    public boolean IsFilled(JTextField TextField, String NomeCampo) {
        if (TextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencher o campo: " + NomeCampo, 
                    "Campo não preenchido", JOptionPane.ERROR_MESSAGE);
            TextField.requestFocus();
            return false;
        }
        else 
            return true;
    }
    
    
    public boolean IsFilled(JTextArea TextArea, String NomeCampo) {
        if (TextArea.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencher o campo: " + NomeCampo, 
                    " Campo não preenchido ", JOptionPane.ERROR_MESSAGE);
            TextArea.requestFocus();
            return false;
        }
        else 
            return true;
    }
    
    
    public boolean IsFilled(JFormattedTextField FormattedTextField, String NomeCampo){
        
        String Campo = FormattedTextField.getText().replace("-", "");//Substitiu por espaço em branco.
        Campo = Campo.replace(".", "");     Campo = Campo.replace("(", "");
        Campo = Campo.replace("/", "");     Campo = Campo.replace(")", "");
        Campo = Campo.replace(",", "");     Campo = Campo.replace("$", "");
        Campo = Campo.replace(":", "");     Campo = Campo.replace("%", "");
        Campo = Campo.replace("!", "");     Campo = Campo.replace("@", "");
        Campo = Campo.replace("*", "");     Campo = Campo.replace("&", "");
        if ( Campo.trim().isEmpty() ){//o trim remove esses campos em branco. 
            JOptionPane.showMessageDialog(null, "Preencher o campo " + NomeCampo, " Campo não Preenchido! ",
                                             JOptionPane.ERROR_MESSAGE);
            
            FormattedTextField.requestFocus();
            return false;
            
        }
        else
            return true;
    }
        
    
    public boolean IsSelected(JComboBox ComboBox, String NomeCampo) {
        if (ComboBox.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(null, "Selecionar uma das opções do campo: " + NomeCampo, 
                    "Campo não preenchido", JOptionPane.ERROR_MESSAGE);
            ComboBox.requestFocus();
            return false;
        }
        else
            return true;
    }    
    
    public boolean IsSelected(ButtonGroup buttonGroup, String NomeCampo) {
        if (buttonGroup.getSelection() == null) {
            JOptionPane.showMessageDialog(null, 
                    "Selecionar uma das opções do campo: " + NomeCampo, 
                    "Campo não prenchido",  JOptionPane.ERROR_MESSAGE
            );            
            return false;
        }
        else
            return true;        
    }
}
