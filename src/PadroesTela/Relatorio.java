/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PadroesTela;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author henriquefari
 */
public class Relatorio {
    
    public void Gerar(JTable Grid, String Titulo) {
        //Definição do documento que será criado
        Document document = new Document(); 
        
        try {           
            //Conversão do documento para pdf que será criado na pasta do .jar
            PdfWriter.getInstance(document, new FileOutputStream("Relatorio.pdf"));
            document.open();
            //Para cada parágrafo inserido deverá instanciar p e dicionar no documento;
            Paragraph p;
            
            //Inserir Título 
            p = new Paragraph();                 
            p.add(new Chunk("Cadastro de " + Titulo + "\n\n", new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD)));                
            p.setAlignment(Element.ALIGN_CENTER);
            document.add(p);  
            
            
            //Cabeçalho do Grid
            p = new Paragraph();    
            for(int i=0; i<Grid.getColumnCount(); i++){//Colunas do grid

                if(i > 0){//Pegando o tamanho da coluna 
                    float tamanho;
                    tamanho = (float) Grid.getColumnModel().getColumn(i-1).getWidth();
                    p.add(Chunk.createTabspace(tamanho));
                }
                p.add(new Chunk(Grid.getColumnName(i), new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD)));
            }
            document.add(p); 
            
            
            //Dados do Grid
            for(int i=0; i<Grid.getRowCount(); i++) {//'LINHAS' do grid
                p = new Paragraph(); 

                //Colunas do grid
                for(int j=0; j<Grid.getColumnCount(); j++) {
                    if(j > 0){
                        float tamanho;
                        tamanho = (float) Grid.getColumnModel().getColumn(j-1).getWidth();
                        p.add(Chunk.createTabspace(tamanho));
                    }
                    p.add(new Chunk(String.valueOf(Grid.getValueAt(i, j)), new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD)));
                }
                document.add(p);
            }
   

        }
        catch(DocumentException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        catch(FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        finally {
            document.close();
        }
        
        //Abrir o arquivo PDF criado
        try {
            Desktop.getDesktop().open(new File("Relatorio.pdf"));
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
}
