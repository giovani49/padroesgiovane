/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import PadroesTela.Grid;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Windows
 */
public class ConexaoMYSQL {
    private Statement stmt; //executar consultas DQL, DML
    private ResultSet rs; //gerenciar consultas DQL
    private Connection c;          
    private int[] FieldTypes;
    
    public ConexaoMYSQL(String Database) {
        String user = "root";
        String password = "";
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/" + Database;
        
        //Abrir conexÃ£o
        try {            
            Class.forName(driver).newInstance();                               
            c = DriverManager.getConnection(url, user, password);            
            stmt = c.createStatement();  
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);            
            JOptionPane.showMessageDialog(null, "A aplicação será finalizada...");
            System.exit(0); //Finalziar aplicaÃ§Ã£o
        }              
    }    

    
    public Statement getStmt() {
        return stmt;
    }
    
    
    public boolean SQLExecute(String SQL) {//Com esse método eu faço as inserções updates e deleções.
        try {
            this.stmt.execute(SQL);
            return true;
        }
        catch(SQLException e) {
            if(e.getErrorCode() == 1451){
                JOptionPane.showMessageDialog(null, "Existem registros que dependem desse registro!"
                        , "Impossível excluir", JOptionPane.ERROR_MESSAGE);
            }
            else if(e.getErrorCode() == 1062){
                JOptionPane.showMessageDialog(null, "Algum campo desse registro já está cadastrado!\n"
                        + "Verifique por exemplo o campo de CPF!"
                    , "Impossível inserir", JOptionPane.ERROR_MESSAGE);
            }
            else if(e.getErrorCode() == 1406){
                JOptionPane.showMessageDialog(null, "Numero de caracteres excedidos!"
                    , "Impossível inserir", JOptionPane.ERROR_MESSAGE);
            }
            else
                JOptionPane.showMessageDialog(null, e);
            return false;
        }        
    }
    
    
    public void setResultSet(String Query) {//com esse método eu retorno as querys 'consultas'.
        try {
            this.rs = this.stmt.executeQuery(Query);
        }
        catch(SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }                
    }  
    

    public ResultSet getResultSet() {
        return rs; //Retorna o resultado da query
    }        

    
    public static Connection getConnection(String Database){
        String user = "root";
        String password = "";
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/" + Database;
        
        try {
            Class.forName(driver);
            return DriverManager.getConnection(url, user, password);
            
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("Erro na conexão:", ex);
        }
    }
    
    
    //Esse metodo é do tipo JComboBox pq preciso usar as funções do ComboBox.
    public JComboBox setComboBox(JComboBox ComboBox, String Table) {//Toda vez que eu cadastrar uma marca eu vou colocar no combo box.
        
        ComboBox.removeAllItems();
        
        if(ComboBox.getName().substring(3).equalsIgnoreCase("Veiculo")){
            this.setResultSet("SELECT * FROM " +Table + " ORDER BY Placa ");
        }
        else if(ComboBox.getName().substring(3).equalsIgnoreCase("Habilitacao")){
            this.setResultSet("SELECT * FROM " +Table + " ORDER BY codCurso ");
        }
        else{
            this.setResultSet("SELECT * FROM " +Table + " ORDER BY Nome ");
        }
        
        try {
            if(ComboBox.getName().substring(3).equalsIgnoreCase("Veiculo")){
                while(this.getResultSet().next()){
                    ComboBox.addItem(this.getResultSet().getString("Placa"));
                }
            }
            else if(ComboBox.getName().substring(3).equalsIgnoreCase("Habilitacao")){
                while(this.getResultSet().next()){
                    ComboBox.addItem(this.getResultSet().getString("codCurso"));
                }
            }
            else{
                while(this.getResultSet().next()){
                    ComboBox.addItem(this.getResultSet().getString("Nome"));
                }
            }
            
            ComboBox.setSelectedIndex(-1);
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoMYSQL.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
        
        return ComboBox;
    }
    
    //Esse método retorna para o combobox uma coluna em especifico da view;
    public JComboBox setComboBoxEspecifico(JComboBox ComboBox, String Table, String Coluna) {
        
        ComboBox.removeAllItems();
        this.setResultSet("SELECT " +Coluna+ " FROM " +Table + " ORDER BY " + Coluna);
        try {
            while(this.getResultSet().next()){
                ComboBox.addItem(this.getResultSet().getString(Coluna));
            }
            ComboBox.setSelectedIndex(-1);
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoMYSQL.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
        
        return ComboBox;
    }
    
    
    public int ObterCodigo(JComboBox ComboBox){
        int Codigo = 0;
        if(ComboBox.getName().substring(3).equalsIgnoreCase("Veiculo")){
            this.setResultSet("SELECT " +ComboBox.getName()+ " FROM " +ComboBox.getName().substring(3)+
                " WHERE Placa = " + " '"+ComboBox.getSelectedItem()+"' ");
        }
        else{
            this.setResultSet("SELECT " +ComboBox.getName()+ " FROM " +ComboBox.getName().substring(3)+
                " WHERE Nome = " + " '"+ComboBox.getSelectedItem()+"' ");
        }
        
        try {
            this.getResultSet().first();
            Codigo = Integer.parseInt(this.getResultSet().getString(ComboBox.getName()));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Codigo;
    }
    
    public int ObterCodigoEspecifico(JComboBox ComboBox, String Coluna){
        int Codigo = 0;
        this.setResultSet("SELECT " +ComboBox.getName()+ " FROM " +ComboBox.getName().substring(3)+
                            " WHERE " +Coluna+ " =  '"+ComboBox.getSelectedItem()+"' ");
        
        try {
            this.getResultSet().first();
            Codigo = Integer.parseInt(this.getResultSet().getString(ComboBox.getName()));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Codigo;
    }
    
    
    public void setInsert(String Consulta) {                                              
         
        String SQL = Consulta ;
        if(SQLExecute(SQL)){
            this.SQLExecute(SQL);//Com esse método eu faço as inserções updates e deleções.

            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso!");
        }
    }
    
    
    public void setDelete(JTable Grid, String Tabela) {                                              
        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(null, "Confirmar ação?", 
                    "Exclusão/Alteração de dados", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            Grid g = new Grid();
            String Codigo = g.getValor(Grid, 0);
            if(!Codigo.isEmpty()){
                String SQL = "DELETE FROM " + Tabela + "  WHERE Cod" +Tabela+ " = '"+Codigo+"' ";

                if(SQLExecute(SQL)){
                    this.SQLExecute(SQL);//Com esse método eu faço as inserções updates e deleções.

                    JOptionPane.showMessageDialog(null, "Exluido com sucesso!");
                }

            }
        }    
    }
    
    
    public void setUpadate(JTable Grid, String Consulta){
        Grid g = new Grid();
        String Codigo = g.getValor(Grid, 0);
        if(!Codigo.isEmpty()){
            String SQL = Consulta + " '"+Codigo+"' ";
            
            if(SQLExecute(SQL)){
                this.SQLExecute(SQL);//Com esse método eu faço as inserções updates e deleções.
            
                JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
            }
        }     
    }
    
    
    public void ConstruirGrid(JTable Table, String[] ColName){
        DefaultTableModel model = new DefaultTableModel(){
        public boolean isCellEditable(int rowIndex, int columnIndex) {  
           return false;  
        }  
        };
        Object[][] dados = {
            
        };
        String[] NomeColuna = ColName;
      
        model.setDataVector(dados, NomeColuna);
        Table.setModel(model);
        
        Table.getColumnModel().getColumn(0).setMinWidth(70);
        Table.getTableHeader().setReorderingAllowed(false);
       /* for(int i=0; i<qtdCol; i++){//
            Table.getColumnModel().getColumn(i).setResizable(false);
        }*/
    }
    
    
    
    public void PreencherGrid(JTable Grid, String view, String[] ColunName){//Get Grid
        DefaultTableModel dtm = new DefaultTableModel();
        dtm = (DefaultTableModel) Grid.getModel();
        
        //Exclui todas as linhas
        //Limpa todo o grid primeiro para que ele possa ser preechido denovo com o novo registro ou com o msm alterado.
        while (dtm.getRowCount() > 0){
            dtm.removeRow(0);
        }
        
        
        //Definir tipos
        this.FieldTypes = new int[Grid.getColumnCount()];
        for(int i=0; i<Grid.getColumnCount(); i++) {
            try {
                String SQL;
                SQL = "SELECT " + ColunName[i] + " FROM " + view;
                
                setResultSet(SQL);                                                       
                ResultSetMetaData rsmd = getResultSet().getMetaData();
                this.FieldTypes[i] = rsmd.getColumnType(1);                    
            }
            catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }//Fim for
        
        
        try {//Abaixo comando.
            this.setResultSet("SELECT * FROM " +view);
            //this.getResultSet().first();
            
            if(this.getResultSet().first()){
                
                do{
                    Object linha[] = new Object[Grid.getColumnCount()];
                    
                    for(int i=0; i<Grid.getColumnCount(); i++){
                        
                        String ValorCampo = this.getResultSet().getString(ColunName[i]);
                        if(getResultSet().wasNull()){
                            linha[i] = "";
                        }
                        else{
                            //Se o campo for do tipo data, definir padrão brasileiro
                            if (this.FieldTypes[i] == Types.DATE || this.FieldTypes[i] == Types.TIMESTAMP) {                                
                                try {                                                                        
                                    Date data = getResultSet().getDate(ColunName[i]);
                                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                                    ValorCampo = formato.format(data);                         
                                } catch (SQLException e) {
                                    JOptionPane.showMessageDialog(null, e);
                                }   
                            }
                            
                            //Se o campo for real
                            if (this.FieldTypes[i] == Types.REAL || this.FieldTypes[i] == Types.DOUBLE || this.FieldTypes[i] == Types.DECIMAL || this.FieldTypes[i] == Types.FLOAT) {
                                ValorCampo = String.format("%.2f", Float.valueOf(ValorCampo));
                            }
                            
                            //Adicionar no grid o valor do campo
                            linha[i] = ValorCampo;
                        }
                        
                    }
                    
                    dtm.addRow(linha);
                }while(this.getResultSet().next());
                
            } 
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    
    public void PreencherGrid02(JTable Grid, String view, String Consulta, String[] ColunName){//Get Grid
        DefaultTableModel dtm = new DefaultTableModel();
        dtm = (DefaultTableModel) Grid.getModel();
        
        
        while (dtm.getRowCount() > 0){
            dtm.removeRow(0);
        }
        
        
        //Definir tipos
        this.FieldTypes = new int[Grid.getColumnCount()];
        for(int i=0; i<Grid.getColumnCount(); i++) {//No preencherGrd de cima eu mando o grid.count.. mas nesse mando o 
            try {                   //o colname pq nem sempre o grid será todo preenchido por esse método.
                String SQL;
                SQL = "SELECT " + ColunName[i] + " FROM " + view;
                
                setResultSet(SQL);                                                       
                ResultSetMetaData rsmd = getResultSet().getMetaData();
                this.FieldTypes[i] = rsmd.getColumnType(1);                    
            }
            catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }//Fim for
        
        
        try {//Abaixo comando.
            this.setResultSet(Consulta);
            //this.getResultSet().first();
            
            if(this.getResultSet().first()){
                
                do{
                    Object linha[] = new Object[Grid.getColumnCount()];
                    
                    for(int i=0; i<Grid.getColumnCount(); i++){
                        
                        String ValorCampo = this.getResultSet().getString(ColunName[i]);
                        if(getResultSet().wasNull()){
                            linha[i] = "";
                        }
                        else{
                            //Se o campo for do tipo data, definir padrão brasileiro
                            if (this.FieldTypes[i] == Types.DATE || this.FieldTypes[i] == Types.TIMESTAMP) {                                
                                try {                                                                        
                                    Date data = getResultSet().getDate(ColunName[i]);
                                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                                    ValorCampo = formato.format(data);                         
                                } catch (SQLException e) {
                                    JOptionPane.showMessageDialog(null, e);
                                }   
                            }
                            
                            //Se o campo for real
                            if (this.FieldTypes[i] == Types.REAL || this.FieldTypes[i] == Types.DOUBLE || this.FieldTypes[i] == Types.DECIMAL || this.FieldTypes[i] == Types.FLOAT) {
                                ValorCampo = String.format("%.2f", Float.valueOf(ValorCampo));
                            }
                            
                            //Adicionar no grid o valor do campo
                            linha[i] = ValorCampo;
                        }
                        
                    }
                    
                    dtm.addRow(linha);
                }while(this.getResultSet().next());
                
            } 
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
  
}
